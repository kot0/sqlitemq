package sqliteMQ

import (
	"fmt"
	"os"
	"strings"
	"sync"
	"testing"

	"github.com/spf13/cast"
	"gitlab.com/kot0/tools"
)

const TmpDBFilePrefix = "TEST_DB_TMP_"

var numberOfTestMessages = []int{1, 10, 100, 1000, 10000}

func init() {
	testClean()

	// Speedtest
	// mq := testMustCreateMQ(true)
	//
	// var messagesProcessed uint64
	// go func() {
	// 	for {
	// 		tmp := atomic.LoadUint64(&messagesProcessed)
	// 		time.Sleep(1 * time.Second)
	// 		fmt.Println("Messages processed per second:", humanize.Comma(int64(atomic.LoadUint64(&messagesProcessed)-tmp)), "processed:", humanize.Comma(int64(messagesProcessed)))
	// 	}
	// }()
	//
	// for {
	// 	var payloads []string
	// 	for i := 0; i < 1000; i++ {
	// 		payloads = append(payloads, tools.RandomStringSlow(16))
	// 	}
	// 	tools.OnErrorPanic(mq.PutMessages(payloads))
	//
	// 	var messages []*DBMessage
	// 	var err error
	//
	// 	for {
	// 		messages, err = mq.GetMessages(1000)
	// 		tools.OnErrorPanic(err)
	// 		tools.OnErrorPanic(mq.DoneMessages(messages))
	// 		atomic.AddUint64(&messagesProcessed, uint64(len(messages)))
	//
	// 		if len(messages) == 0 {
	// 			break
	// 		}
	// 	}
	// }
}

func (mq *MQ) benchmarkReadWrite(numberOfTestMessages int) {
	var payloads []string
	for i := 0; i < numberOfTestMessages; i++ {
		payloads = append(payloads, tools.RandomStringSlow(16))
	}
	tools.OnErrorPanic(mq.PutMessages(payloads))

	messages, err := mq.GetMessages(numberOfTestMessages)
	tools.OnErrorPanic(err)

	// fmt.Println("len(messages):", len(messages))
	for _, message := range messages {
		if !tools.SliceContains(payloads, message.Payload) {
			panic(`!tools.SliceContains(payloads, message.Payload)`)
		}
	}

	tools.OnErrorPanic(mq.DoneMessages(messages))
}

func (mq *MQ) benchmarkWrite(numberOfTestMessages int) {
	var payloads []string
	for i := 0; i < numberOfTestMessages; i++ {
		payloads = append(payloads, tools.RandomStringSlow(16))
	}
	tools.OnErrorPanic(mq.PutMessages(payloads))
}

func BenchmarkMQReadWrite(b *testing.B) {
	for _, numberOfTestMessages := range numberOfTestMessages {
		mq := testMustCreateMQ(true)
		b.Run("BenchmarkMQReadWrite/"+cast.ToString(numberOfTestMessages), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				mq.benchmarkReadWrite(numberOfTestMessages)
			}
		})
		tools.OnErrorPanic(mq.Close())
	}
}

func testClean() {
	files, err := os.ReadDir(".")
	tools.OnErrorPanic(err)
	for _, file := range files {
		if file.IsDir() {
			continue
		}

		if strings.HasPrefix(file.Name(), TmpDBFilePrefix) {
			fmt.Println("Remove tmp db:", file.Name())
			tools.OnErrorPanic(os.Remove(file.Name()))
		}
	}
}

func testMustCreateMQ(randomDBName bool) *MQ {
	dbPath := TmpDBFilePrefix

	if randomDBName {
		dbPath += tools.RandomStringSlow(16)
	} else {
		dbPath += "1"
	}

	dbPath += ".db"

	mq, err := NewMQ(&Config{
		DbPath: dbPath,
		AfterOpeningSetStatusOfAllMessagesToInQueue: true,
	})
	tools.OnErrorPanic(err)
	return mq
}

func TestMQReadWrite(t *testing.T) {
	for _, numberOfTestMessages := range numberOfTestMessages {
		t.Run("TestMQReadWrite/"+cast.ToString(numberOfTestMessages), func(t *testing.T) {
			mq := testMustCreateMQ(true)
			mq.benchmarkReadWrite(numberOfTestMessages)
			tools.OnErrorPanic(mq.Close())
		})
	}
}

func TestMQWriteAndCount(t *testing.T) {
	for _, numberOfTestMessages := range numberOfTestMessages {
		t.Run("TestMQWriteAndCount/"+cast.ToString(numberOfTestMessages), func(t *testing.T) {
			mq := testMustCreateMQ(true)
			mq.benchmarkWrite(numberOfTestMessages)

			count, err := mq.CountOfMessages()
			tools.OnErrorPanic(err)

			if numberOfTestMessages != count {
				fmt.Println("numberOfTestMessages:", numberOfTestMessages, "count:", count)
				panic("numberOfTestMessages != count")
			}

			tools.OnErrorPanic(mq.Close())
		})
	}
}

func TestMQReadWriteWithMQReopen(t *testing.T) {
	for _, numberOfTestMessages := range numberOfTestMessages {
		t.Run("TestMQReadWriteWithMQReopen/"+cast.ToString(numberOfTestMessages), func(t *testing.T) {
			for i := 0; i < 10; i++ {
				mq := testMustCreateMQ(false)
				mq.benchmarkReadWrite(numberOfTestMessages)
				tools.OnErrorPanic(mq.Close())
			}
		})
	}
}

var testMQReadWriteSimpleDataset = []string{
	"a1",
	"a2",
	"b1",
	"b2",
	"c1",
	"c2",
	"d1",
	"d2",
	"e1",
	"e2",
}

func TestMQReadWriteSimple(t *testing.T) {
	mq := testMustCreateMQ(true)
	payloads := testMQReadWriteSimpleDataset

	tools.OnErrorPanic(mq.PutMessages(payloads))

	messages, err := mq.GetMessages(100)
	tools.OnErrorPanic(err)

	// fmt.Println("len(messages):", len(messages))
	for _, message := range messages {
		// fmt.Println(message)
		if !tools.SliceContains(payloads, message.Payload) {
			panic(`!tools.SliceContains(payloads, message.Payload)`)
		}
	}

	tools.OnErrorPanic(mq.DoneMessages(messages))

	tools.OnErrorPanic(mq.Close())
}

func TestMQReadWriteSimpleMultiGoroutine(t *testing.T) {
	mq := testMustCreateMQ(true)
	payloads := testMQReadWriteSimpleDataset

	tools.OnErrorPanic(mq.PutMessages(payloads))

	var resultMessages []*DBMessage
	var wg sync.WaitGroup
	var syncWg sync.WaitGroup
	syncWg.Add(1)

	for i := 0; i < len(payloads); i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			syncWg.Wait()

			messages, err := mq.GetMessages(1)
			tools.OnErrorPanic(err)

			tools.OnErrorPanic(mq.DoneMessages(messages))

			resultMessages = append(resultMessages, messages...)
		}()
	}

	syncWg.Done()
	wg.Wait()

	var unique = make(map[uint64]uint64)
	for _, message := range resultMessages {
		unique[message.Id] += 1

		// fmt.Println(message)
		if !tools.SliceContains(payloads, message.Payload) {
			panic(`!tools.SliceContains(payloads, message.Payload)`)
		}
	}

	for key, val := range unique {
		// fmt.Println(cast.ToString(key)+":", val)
		if val != 1 {
			panic(cast.ToString(key) + ": " + `val != 1`)
		}
	}

	tools.OnErrorPanic(mq.Close())
}
