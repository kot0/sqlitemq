module gitlab.com/kot0/sqlitemq

go 1.18

require (
	github.com/jmoiron/sqlx v1.3.5
	github.com/mattn/go-sqlite3 v1.14.15
	github.com/spf13/cast v1.5.0
	gitlab.com/kot0/tools v1.8.1
)

require github.com/Pallinder/go-randomdata v1.2.0 // indirect
