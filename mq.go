package sqliteMQ

import (
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/spf13/cast"

	"gitlab.com/kot0/tools"

	_ "github.com/mattn/go-sqlite3"
)

// dbSchema mq sqlite schema
const dbSchema string = `
CREATE TABLE IF NOT EXISTS "messages" (
	"id" INTEGER,
	"payload" TEXT,
	"status" TEXT,
	"put_time_unix" INTEGER,
	PRIMARY KEY("id" AUTOINCREMENT)
);

CREATE INDEX IF NOT EXISTS "status_index" ON "messages" (
	"status" DESC
);
`

// SqliteMaxVariableNumber sqlite max variable number
const SqliteMaxVariableNumber = 32766

// Config struct
type Config struct {
	DbPath                                      string
	AfterOpeningSetStatusOfAllMessagesToInQueue bool
}

// MQ struct
type MQ struct {
	db *sqlx.DB
}

// NewMQ creates new MQ from *Config
func NewMQ(config *Config) (*MQ, error) {
	// Open sqlite
	db, err := sqlx.Open("sqlite3", config.DbPath)
	if tools.CheckError(err) {
		return nil, err
	}
	db.SetMaxOpenConns(1)
	db.SetMaxIdleConns(1)
	db.SetConnMaxLifetime(10 * time.Second)
	db.SetConnMaxIdleTime(10 * time.Second)

	// Enable WAL
	_, err = db.Exec(`
PRAGMA journal_mode = WAL;
PRAGMA synchronous = NORMAL;
`)

	// Exec dbSchema
	_, err = db.Exec(dbSchema)
	if tools.CheckError(err) {
		return nil, err
	}

	// After opening set status of all messages to in_queue
	if config.AfterOpeningSetStatusOfAllMessagesToInQueue {
		_, err = db.Exec(`update "messages" set "status" = 'in_queue' where "status" = 'processing'`)
		if tools.CheckError(err) {
			return nil, err
		}
	}

	return &MQ{db: db}, err
}

// Close MQ
func (mq *MQ) Close() error {
	return mq.db.Close()
}

// DBMessage struct
type DBMessage struct {
	Id          uint64 `db:"id"`
	Payload     string `db:"payload"`
	Status      string `db:"status"`
	PutTimeUnix int64  `db:"put_time_unix"`
	PutTime     time.Time
}

// PutMessages to MQ
func (mq *MQ) PutMessages(payloads []string) error {
	messages := mq.generateDBMessagesFromPayloads(payloads)

	for _, messagesChunk := range tools.ChunkSlice(messages, SqliteMaxVariableNumber/3) {
		tx, err := mq.db.Beginx()
		if tools.CheckError(err) {
			return err
		}

		_, err = tx.NamedExec(`insert into "messages" ("payload", "status", "put_time_unix") values (:payload, :status, :put_time_unix)`, messagesChunk)
		if tools.CheckError(err) {
			return err
		}

		err = tx.Commit()
		if tools.CheckError(err) {
			return err
		}
	}

	return nil
}

// GetMessages from MQ with limit
func (mq *MQ) GetMessages(limit int) ([]*DBMessage, error) {
	tx, err := mq.db.Beginx()
	if tools.CheckError(err) {
		return nil, err
	}

	var messages []*DBMessage
	err = tx.Select(&messages, `select * from "messages" where "status" = 'in_queue' order by "id" limit ?`, limit)
	if tools.CheckError(err) {
		return nil, err
	}

	// Convert unix time to time.Time in messages
	for _, message := range messages {
		message.PutTime = time.Unix(message.PutTimeUnix, 0)
	}

	err = mq.SetMessagesStatus(tx, messages, "processing")
	if tools.CheckError(err) {
		return nil, err
	}

	err = tx.Commit()
	if tools.CheckError(err) {
		return nil, err
	}

	return messages, nil
}

// generateDBMessageFromMessagePayload for MQ
func (mq *MQ) generateDBMessageFromMessagePayload(payload string) *DBMessage {
	timeNow := time.Now()
	return &DBMessage{Payload: payload, Status: "in_queue", PutTimeUnix: timeNow.Unix(), PutTime: timeNow}
}

// DoneMessages Removes messages from MQ
func (mq *MQ) DoneMessages(messages []*DBMessage) error {
	for _, messagesChunk := range tools.ChunkSlice(messages, SqliteMaxVariableNumber/1) {
		tx, err := mq.db.Beginx()
		if tools.CheckError(err) {
			return err
		}

		query := strings.ReplaceAll(`delete from "messages" where "id" in(?)`, `?`, strings.Join(dBMessagesGetIdsInString(messagesChunk), ","))
		// fmt.Println("query:", query)
		_, err = tx.Exec(query)
		if tools.CheckError(err) {
			return err
		}

		err = tx.Commit()
		if tools.CheckError(err) {
			return err
		}
	}

	return nil
}

// SetMessagesStatus in MQ
func (mq *MQ) SetMessagesStatus(tx *sqlx.Tx, messages []*DBMessage, status string) error {
	for _, messagesChunk := range tools.ChunkSlice(messages, SqliteMaxVariableNumber/1) {
		for _, message := range messagesChunk {
			message.Status = status
		}

		query := strings.ReplaceAll(`update "messages" set "status" = ? where "id" in('?')`, `'?'`, strings.Join(dBMessagesGetIdsInString(messagesChunk), ","))
		// fmt.Println("query:", query)
		_, err := tx.Exec(query, status)
		if tools.CheckError(err) {
			return err
		}
	}

	return nil
}

// generateDBMessagesFromPayloads for MQ
func (mq *MQ) generateDBMessagesFromPayloads(payloads []string) (messages []*DBMessage) {
	for _, payload := range payloads {
		messages = append(messages, mq.generateDBMessageFromMessagePayload(payload))
	}
	return
}

// dBMessagesGetIdsInString extract messages id to array of strings
func dBMessagesGetIdsInString(messages []*DBMessage) []string {
	var ids []string
	for _, message := range messages {
		ids = append(ids, cast.ToString(message.Id))
	}
	return ids
}

// CountOfMessages get count of messages in MQ
func (mq *MQ) CountOfMessages() (int, error) {
	tx, err := mq.db.Beginx()
	if tools.CheckError(err) {
		return 0, err
	}

	var count int
	rows, err := tx.Query(`select count(*) from "messages"`)
	if tools.CheckError(err) {
		return 0, err
	}
	for rows.Next() {
		err := rows.Scan(&count)
		if tools.CheckError(err) {
			return 0, err
		}
	}

	err = tx.Commit()
	if tools.CheckError(err) {
		return 0, err
	}

	return count, nil
}
