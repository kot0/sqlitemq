# sqliteMQ simple message queue lib based on sqlite

## Example:

```go
package main

import (
	"fmt"

	"gitlab.com/kot0/sqlitemq"
)

func main() {
	// Create new mq
	mq, err := sqliteMQ.NewMQ(&sqliteMQ.Config{
		DbPath: "mq.db",
		AfterOpeningSetStatusOfAllMessagesToInQueue: true,
	})
	if err != nil {
		panic(err)
	}

	err = mq.PutMessages([]string{
		"a1",
		"a2",
		"b1",
	})
	if err != nil {
		panic(err)
	}

	messages, err := mq.GetMessages(100)
	if err != nil {
		panic(err)
	}

	for _, message := range messages {
		fmt.Println(message)
	}

	// Delete messages from mq
	err = mq.DoneMessages(messages)
	if err != nil {
		panic(err)
	}

	// Close mq
	err = mq.Close()
	if err != nil {
		panic(err)
	}
}
```
